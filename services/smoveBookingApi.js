const apiRoute = 'https://challenge.smove.sg/availability?';
const axios = require('axios');

const getBookingAvaliability = (startTime, endTime) => {
  // GET https://challenge.smove.sg/availability?startTime=[startTime]&endTime=[endTime]
  return axios.get(`${apiRoute}startTime=${startTime}&endTime=${endTime}`)
    .then(({ data: response }) => {
      const { data } = response;
      return data;
    })
    .catch(({ response }) => {
      const err = response.data;
      throw err;
    })
}

module.exports = {
  getBookingAvaliability
};