const { googleMapsApiKey: key } = require('../config/environment');

const geocoding = new require('reverse-geocoding-google');
const config = {
  'key': key,
};

const reverseGeoCode = (coordinates) => {
  const [latitude, longitude] = coordinates;
  const parameters = {
    latitude,
    longitude,
    ...config,
  }
  return new Promise((resolve, reject) => {
    geocoding.location(parameters, function (err, data) {
      if (err) {
        throw err;
      } else{
        if (!data) {
          resolve(`${latitude}, ${longitude}`);
        }
        const {results} = data;
        const [first] = results;
        resolve(first.formatted_address);
      }
    })
  })
  .catch((err) => {
    console.log(`rejected: ${err}`);
    throw new HttpError(500, err)
  });
}

module.exports = {
  reverseGeoCode,
}
