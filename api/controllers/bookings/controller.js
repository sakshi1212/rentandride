const asyncMW = require('../../../middleware/async');
const { getBookingAvaliability } = require('../../../services/smoveBookingApi');
const moment = require('moment');
const toString = require('lodash/toString');
const { reverseGeoCode } = require('../../../services/googleMapsApi');
const { maxLocationOptions } = require('../../../config/environment');


exports.index = asyncMW(async (req, res) => {
  try {
    const bookings = await DB.Booking.findAll();
    return res.status(200).send({
      data: bookings,
    });
  } catch (err) {
    console.log(err);
    throw new HttpError(500, err.message);
  }
});

exports.create = asyncMW(async(req, res) => {
  try {
    const createdBooking = await DB.Booking.create(req.body);
    return res.status(200).json({ data: createdBooking});
  } catch (err) {
    console.log(err);
    throw new HttpError(500, err.message);
  }
});

const getAddress = async(option) => {
  const { location } = option;
  const address = await reverseGeoCode(location);;
  return {
    ...option,
    locationAddress: address,
    locationStr: toString(option.location),
  }
};

exports.getAvaliability = asyncMW(async(req, res) => { 
  const {
    startTime,
    endTime
  } = req.query;

  const startTimeQuery = moment(startTime).unix();
  const endTimeQuery = moment(endTime).unix();
  try {
    const bookingOptions = await getBookingAvaliability(startTimeQuery, endTimeQuery);
    const bookingOptionsFiltered = bookingOptions.filter(b => b.available_cars !== 0);
    const cappedBookingOptions = bookingOptionsFiltered.slice(0, maxLocationOptions);
    const formattedBookingOptions = await Promise.all(cappedBookingOptions.map(op => getAddress(op)));
    return res.status(200).json(formattedBookingOptions);
  } catch (err) {
    console.log(err);
    throw new HttpError(500, err.message);
  }
})

exports.getDropOffLocations = asyncMW(async(req, res) => {
  const {
    startTime,
    endTime,
    startLocationSelected
  } = req.query;
  const startTimeQuery = moment(startTime).unix();
  const endTimeQuery = moment(endTime).unix();
  try {
    const bookingOptions = await getBookingAvaliability(startTimeQuery, endTimeQuery);
    const [bookingOptionSelected] = bookingOptions.filter(b => b.available_cars !== 0 && toString(b.location) === startLocationSelected);
    const { dropoff_locations : dropoffLocations } = bookingOptionSelected;
    const cappedBookingOptions = dropoffLocations.slice(0, maxLocationOptions);
    const formattedBookingOptions = await Promise.all(cappedBookingOptions.map(op => getAddress(op)));
    return res.status(200).json(formattedBookingOptions);
  } catch (err) {
    console.log(err);
    throw new HttpError(500, err.message);
  }
})

