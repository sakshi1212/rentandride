const express = require('express');
const controller = require('./controller');

const router = express.Router();

router.get('/', controller.index);
router.post('/', controller.create);
router.get('/getAvaliability', controller.getAvaliability);
router.get('/getDropOffLocations', controller.getDropOffLocations);

module.exports = router;
