const Sequelize = require('sequelize');

const { DataTypes } = Sequelize;

class Booking extends Sequelize.Model {

  static init(sequelize) {
    return super.init(
      {
        id: {
          allowNull: false,
          autoIncrement: true,
          primaryKey: true,
          type: DataTypes.INTEGER,
        },
        email: {
          type: DataTypes.STRING,
          allowNull: false,
        },
        name: {
          type: DataTypes.STRING,
          allowNull: false,
        },
        phoneNum: {
          type: DataTypes.STRING,
          allowNull: false,
        },
        startTime: {
          allowNull: false,
          type: DataTypes.DATE,
        },
        endTime: {
          allowNull: false,
          type: DataTypes.DATE,
        },
        startLocation: {
          type: DataTypes.STRING,
          allowNull: false,
        },
        endLocation: {
          type: Sequelize.STRING,
          allowNull: false,
        },
        startLocationAddress: {
          type: DataTypes.STRING,
          allowNull: false,
        },
        endLocationAddress: {
          type: DataTypes.STRING,
          allowNull: false,
        },
        noOfCars: {
          type: DataTypes.STRING,
          allowNull: false,
        },
      },
      {
        sequelize,
        modelName: 'bookings',
      }
    );
  }

  static associate(models) {
  }
}

module.exports = Booking;
