# rentAndRide

### Summary

Simple website that allows you to create a booking to rent cars, as well as list all bookings made.

### Pre-requistes

1. Node v8.12.0  
2. React ^16.9.0  
3. MySQL  

### Config Variables setup

1. Go to the folder : `./config/environment`  
2. To run locally, set up a `default-env.js` file   
3. Set up the local DB config, and google API key(GeoCoder API) as per the sample below. You may also use the sample as it is. 

`process.env.NODE_ENV = 'development';`  
`process.env.PORT = '8080'`   
`process.env.DB_NAME = 'rentAndRide';  `  
`process.env.DB_HOST = '127.0.0.1';  `  
`process.env.DB_USER = 'root';  `  
`process.env.DB_PASSWORD = '123123123';  `  
`process.env.GOOGLE_MAPS_KEY = "AIzaSyCbVlsO9KNKCR10SyqrUIYuGTUN2P9vNYo"; `   
`process.env.MAX_LOCATION_OPTIONS = '3';`  

4. NOTE : `MAX_LOCATION_OPTIONS` determines how many location options are shown to the user. Defaults to 2.   
5. To replicate an environment locally, Environment specific config can be put into the respective config file ie. staging.js, production.js.  


### Local Installation and Start Server

To install locally, for Server : At root directory  
1. `npm install` to install the libraries  
2. `npm i -g sequelize-cli` to allow us to use sequelize cli  
3. `sequelize db:create` will create the DB using the config provided  
4. `sequelize db:migrate` will migrate the required tables  
**The above 4 steps can also be run by a single line, to save time use : `npm run setup`  **  

After installation, to start server, at root directory run:   
1. `npm run start`  

To run with nodemon for local development, use  
1. `npm run start:dev`  

The server will now be running at port 8080.  


### Local Installation and Start Client  

To start client, Inside `/client` folder  
1. run `npm install` to install the libraries for the client  
2. `npm run start` to start.   

The app is now accessible at http://localhost:3000  

### Deployed At Heroku  

link: https://rent-and-ride.herokuapp.com/  


