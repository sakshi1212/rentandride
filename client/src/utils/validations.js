import moment from 'moment';
import isBoolean from 'lodash/isBoolean';
import isEmpty from 'lodash/isEmpty';

const required = (value) => {
  if (isBoolean(value)) return false;
  if (typeof value === 'number' && value) return false;
  return isEmpty(value);
};

const isNotEmail = (value) => {
  const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return !re.test(value);
};

const isNotDate = (value) => {
  return (moment(value).isValid());
};

export {
  required,
  isNotEmail,
  isNotDate,
};
