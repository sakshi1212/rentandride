import React, { Component } from 'react';
import styled from 'styled-components';
import TimePreferences from '../../components/templates/TimePreferences';
import LocationPreferences from '../../components/templates/LocationPreferences';
import UserDetails from '../../components/templates/UserDetails';
import Confirmation from '../../components/templates/Confirmation';
import Success from '../../components/templates/Success';
import Button from '../../components/atoms/Button';

const FormWrapper = styled.div`
  background: #F4F6F8;
  min-height: 90vh;
  padding-top: 50px;
`

const StepWrapper = styled.div`
  margin: 60px 0px;
  border: 1px solid #dcdada;
  border-radius: 4px;
  background: white;
  padding: 50px;
`

class SelectTime extends Component {
  constructor(props){
    super(props);
    this.state = {
      step: 1,
      name: '',
      email: '',
      phoneNum: '',
      startTime: null,
      endTime: null,
      startLocation: null,
      startLocationAddress: null,
      noOfCars: null,
      endLocation: null,
      startLocationOptions: [],
      endLocationAddress: null,
    }
    this.renderStep = this.renderStep.bind(this)
  }

  nextStep = () => {
    const { step } = this.state
    this.setState({
      step : step + 1
    })
  }

  prevStep = () => {
    const { step } = this.state
    this.setState({
      step : step - 1
    })
  }

  handleChange = (input, action) => event => {
    const value = event.target.value  || event.target.textContent;
    this.setState({ [input] : value })
    if (action) {
      action(value);
    }
  }

  handleDateTimeChange = (event, {name, value}) => {
    if (this.state.hasOwnProperty(name)) {
      this.setState({ [name]: value });
    }
  }

  handleSetStartLocation = (value) => {
    this.setState({
      startLocation: value,
    })
  }

  handleSetEndLocation = (value) => {
    this.setState({
      endLocation: value,
    })
  }

  componentDidMount() {
  }

  renderStep = (step, values) => {
    switch(step) {
      case 1:
        return <TimePreferences 
          nextStep={this.nextStep} 
          handleChange = {this.handleDateTimeChange}
          values={values}
          />
      case 2:
        return <LocationPreferences 
          nextStep={this.nextStep}
          prevStep={this.prevStep}
          handleChange={this.handleChange}
          setStartLocation={this.handleSetStartLocation} 
          setEndLocation={this.handleSetEndLocation}
          values={values}
          />
      case 3:
        return <UserDetails 
          nextStep={this.nextStep}
          prevStep={this.prevStep}
          handleChange = {this.handleChange}
          values={values}
          />
      case 4:
        return <Confirmation 
          nextStep={this.nextStep}
          prevStep={this.prevStep}
          values={values}
          />
      case 5:
        return <Success />
      default: 
        return <TimePreferences 
          nextStep={this.nextStep} 
          handleChange = {this.handleDateTimeChange}
          values={values}
          />
      }
      
  }

  render(){
    const { history } = this.props;
    const {
      step,
      name,
      email,
      phoneNum,
      startTime,
      endTime,
      startLocation,
      noOfCars,
      endLocation,
      startLocationOptions,
      startLocationAddress,
      endLocationAddress,
    } = this.state;
    const values = { name, email, phoneNum, startTime, endTime, startLocation, noOfCars, endLocation, startLocationOptions, startLocationAddress, endLocationAddress };
    return (
      <FormWrapper className="App-padding">
        <Button
          onClick={() => history.push('/')}
          icon="arrow alternate circle left"
          labelPosition="left"
        >
          Back to Home
        </Button>
        <StepWrapper>
          {this.renderStep(step, values)}
        </StepWrapper>
      </FormWrapper>
    )
  }
}

export default SelectTime;
