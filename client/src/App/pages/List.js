import React, { Component } from 'react';
import { Icon, Header, Table } from 'semantic-ui-react'
import moment from 'moment'
import ErrorBlock from '../../components/molecules/ErrorBlock';
import styled from 'styled-components';
import Button from '../../components/atoms/Button';

const ListPageWrapper = styled.div`
  background: #F4F6F8;
  min-height: 90vh;
  padding-top: 50px;
`

const ListWrapper = styled.div`
  margin: 60px 0px;
  border: 1px solid #dcdada;
  border-radius: 4px;
  background: white;
  padding: 50px;
`

class Booking extends Component {
  constructor(props){
    super(props);
    this.state = {
      bookings: [],
      totalCount: 0,
    }
  }

  componentDidMount() {
    this.getBookings();
  }

  getBookings = () => {
    fetch('/bookings')
    .then(res => res.json())
    .then(({ data }) => this.setState({
      bookings: data,
      totalCount: data.length,
    }))
  }

  render() {
    const { bookings, totalCount } = this.state;
    const { history } = this.props;
    return (
      <ListPageWrapper className="App-padding">
        <Button
          onClick={() => history.push('/')}
          icon="arrow alternate circle left"
          labelPosition="left"
        >
          Back to Home
        </Button>
        <ListWrapper>
          <h1>List of Bookings</h1>
          {bookings.length ? (
            <Table celled striped>
              <Table.Header>
                <Table.Row>
                  <Table.HeaderCell>Id</Table.HeaderCell>
                  <Table.HeaderCell>Person</Table.HeaderCell>
                  <Table.HeaderCell>Start Location</Table.HeaderCell>
                  <Table.HeaderCell>End Location</Table.HeaderCell>
                  <Table.HeaderCell>No of Cars</Table.HeaderCell>
                  <Table.HeaderCell>Start Time</Table.HeaderCell>
                  <Table.HeaderCell>End Time</Table.HeaderCell>
                </Table.Row>
              </Table.Header>
          
              <Table.Body>
                {
                  bookings.map(booking => {
                    return (
                      <Table.Row key={booking.id}>
                        <Table.Cell>{booking.id}</Table.Cell>
                        <Table.Cell>
                          <Header as='h5' image>
                            <Icon name="user"/>
                            <Header.Content>
                              {booking.name}
                              <Header.Subheader>{booking.email}<br/>{booking.phoneNum}</Header.Subheader>
                            </Header.Content>
                          </Header>
                        </Table.Cell>
                        <Table.Cell>
                          <Header as='h5' image>
                            <Header.Content>
                              {booking.startLocationAddress}
                              <Header.Subheader>{booking.startLocation}</Header.Subheader>
                            </Header.Content>
                          </Header>
                        </Table.Cell>
                        <Table.Cell>
                          <Header as='h5' image>
                            <Header.Content>
                              {booking.endLocationAddress}
                              <Header.Subheader>{booking.endLocation}</Header.Subheader>
                            </Header.Content>
                          </Header>
                        </Table.Cell>
                      
                        <Table.Cell>{booking.noOfCars}</Table.Cell>
                        <Table.Cell>{moment(booking.startTime).format('MMM-DD-YYYY HH:mm')}</Table.Cell>
                        <Table.Cell>{moment(booking.endTime).format('MMM-DD-YYYY HH:mm')}</Table.Cell>
                      </Table.Row>
                    )
                    
                  })
                }           
              </Table.Body>         
              <Table.Footer>
                <Table.Row>
                  <Table.HeaderCell colSpan='7'>
                    Total : {totalCount} results
                  </Table.HeaderCell>
                </Table.Row>
              </Table.Footer>
            </Table>
          ) : (
            <div>
              <ErrorBlock errorMessage='No Bookings found !' />
            </div>
          )
        }
        </ListWrapper>
      </ListPageWrapper>
    );
  }
}

export default Booking;