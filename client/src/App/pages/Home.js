import React, { Component } from 'react';
import styled from 'styled-components';

import Button from '../../components/atoms/Button';
import homeBackground from '../../assets/images/background.jpg';


const HomeWrapper = styled.div`
  background-image: url(${homeBackground});
  background-size: cover;
  background-repeat: no-repeat;
  background-position: bottom;
  height: 70vh;
`

const HeadingWrapper = styled.div`
  position: absolute;
  top: 15vh;
`

const StyledHeading = styled.h1`
  font-size: 32px;
  font-weight: bold;
  background: #FFEF09;
  width: fit-content;
  padding: 4px 6px;
`

const StyledSubheading = styled.h2`
  font-weight: bold;
  background: #585F67;
  width: fit-content;
  color: white;
  padding: 4px 6px;
`

const ButtonWrapper = styled.div`
  -webkit-box-pack: center;
  -webkit-justify-content: center;
  -ms-flex-pack: center;
  display: flex;
  justify-content: center;
  position: absolute;
  width: calc(100% - 200px);
  top: 65vh;
`

const StyledButton = styled(Button)`
  height: 100px;
  width: 200px;
  &.newBookingButton {
    background: #FFF104 !important;
  }
`

class Home extends Component {
  render() {
    const {
      history
    } = this.props;

    return (
    <HomeWrapper className="App-padding">
      <HeadingWrapper>
        <StyledHeading>Rent and Ride</StyledHeading>
        <StyledSubheading>Rent a car.</StyledSubheading>
        <StyledSubheading>Pick up Anywhere. Drop off Anywhere</StyledSubheading>
      </HeadingWrapper>
      <ButtonWrapper>
        <StyledButton
          className="newBookingButton"
          icon="arrow alternate circle right"
          labelPosition="right"
          onClick={() => history.push('/bookings/create')}
        >
          Create a new Booking
        </StyledButton>
        <StyledButton
          className="listBookingButton" 
          icon="arrow alternate circle right"
          labelPosition="right"
          onClick={() => history.push('/bookings/list')}
        >
          List all Bookings
        </StyledButton>
      </ButtonWrapper>
    </HomeWrapper>
    );
  }
}
export default Home;

