import React, { Component, Fragment } from 'react';
import { Route, Switch } from 'react-router-dom';
import styled from 'styled-components';
import './App.css';
import Home from './pages/Home';
import List from './pages/List';
import BookingForm from './pages/MainForm';
import Footer from '../components/organisms/Footer';
// import homeBackground from '../assets/images/homeBackground.jpg';

const AppContent = styled.div`
  min-height: calc(90vh);
  display: flex;
  flex-direction: column;
  min-width: 375px;
  max-width: 1440px;
  margin: 0 auto;
  @media (min-width: 640px) {
  }
`

class App extends Component {
  render() {
    const App = () => (
      <Fragment>
        <AppContent>
          <Switch>
            <Route exact path='/' component={Home}/>
            <Route path='/bookings/create' component={BookingForm}/>
            <Route path='/bookings/list' component={List}/>
          </Switch>
        </AppContent>
        <Footer/>
      </Fragment>
    )
    return (
      <Switch>
        <App/>
      </Switch>
    );
  }
}

export default App;