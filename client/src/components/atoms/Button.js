import React from 'react';
import { Button as SUIButton, Icon } from 'semantic-ui-react';
import styled from 'styled-components';

const StyledButton = styled(SUIButton)`
  &&& {
    background-color: #FBDC06;
    border-radius: 0px;
  }
`;

const Button = ({
  icon,
  labelPosition,
  children,
  ...props
}) => {
  return (
    <StyledButton
      icon={icon ? true : false}
      labelPosition={labelPosition}
      {...props}
    >
      {icon && <Icon name={icon} />}
      {children && children}
    </StyledButton>
  );
};

export default Button;
