import React, { Component } from 'react';
import { Form } from 'semantic-ui-react';
import filter from 'lodash/filter';
import styled from 'styled-components';
import axios from 'axios';
import Select from '../molecules/Select';
import Button from '../../components/atoms/Button';
import { required } from '../../utils/validations';
import ErrorBlock from '../../components/molecules/ErrorBlock';

const ButtonWrapper = styled.div`
  margin-top: 50px;
`

class LocationPreferences extends Component{
  constructor(props){
    super(props);
    this.state = {
      bookingOptions: [],
      numberOfCarsOptions: [],
      dropOffLocationOptions: [],
      startLocationOptionsLoading: false,
      numberOfCarsOptionsLoading: false,
      dropOffLocationOptionsLoading: false,
      formError: '',
    }
  }

  handleStartLocationChange = (value) => {
    if (value) {
      const { setStartLocation, values } = this.props;
      this.setState({
        numberOfCarsOptionsLoading: true,
        dropOffLocationOptionsLoading: true,
      });
      const [selectedOption] = filter(this.state.bookingOptions, o => o.locationAddress === value);
      const {available_cars: availableCars, locationStr } = selectedOption;

      setStartLocation(locationStr);
      
      axios.get('/bookings/getDropOffLocations', {
        params: {
          startTime: values.startTime,
          endTime: values.endTime,
          startLocationSelected: locationStr,
        }
      })
      .then(({ data }) => {
        this.setState({
          numberOfCarsOptions: [...Array(availableCars+1).keys()].slice(1),
          dropOffLocationOptions: data,
          numberOfCarsOptionsLoading: false,
          dropOffLocationOptionsLoading: false,
        })
      })
      .catch((err) => {
        console.log(err);
        this.setState({
          formError: `Error Loading Options: ${err}`,
        });
      });
    }
  }

  handleEndLocationChange = (value) => {
    if (value) {
      const { setEndLocation } = this.props;
      const [selectedOption] = filter(this.state.dropOffLocationOptions, o => o.locationAddress === value);
      const { locationStr } = selectedOption;
      setEndLocation(locationStr);
    }
  }

  saveAndContinue = (e) => {
    e.preventDefault();
    const {
      startLocationAddress,
      noOfCars,
      endLocationAddress,
    } = this.props.values;
    if(required(startLocationAddress)) {
      this.setState({
        formError: 'Start Location is Required',
      });
    } else if (required(noOfCars)) {
      this.setState({
        formError: 'Please select number of cars',
      });
    } else if (required(endLocationAddress)) {
      this.setState({
        formError: 'End Location is Required',
      });
    } else {
      this.props.nextStep();
    }
  }

  back  = (e) => {
    e.preventDefault();
    this.props.prevStep();
  }

  componentDidMount() {
    this.setState({
      startLocationOptionsLoading: true,
    });
    const { values } = this.props;
    const {
      startTime, 
      endTime
    } = values;

    axios.get('/bookings/getAvaliability', {
      params: {
        startTime,
        endTime,
      }
    })
    .then(({ data }) => {
      this.setState({
        bookingOptions: data,
        startLocationOptionsLoading: false,
      })
    })
    .catch((err) => {
      console.log(err);
      this.setState({
        formError: `Error Loading Options. Please check start and end dates`,
      });
    });
  }

  render(){
    const { values } = this.props;
    const {
      bookingOptions,
      numberOfCarsOptions,
      dropOffLocationOptions,
      startLocationOptionsLoading,
      numberOfCarsOptionsLoading,
      dropOffLocationOptionsLoading,
      formError,
    } = this.state;
    const startLocationOptions = bookingOptions.map(b => ({ value: b.locationStr, text: b.locationAddress })); 
    const endLocationOptions = dropOffLocationOptions.map(b => ({ value: b.locationStr, text: b.locationAddress }));
    return(
    <Form color='blue' >
      <h1 className="ui centered">Enter Location Preferences</h1>
      {formError && <ErrorBlock errorMessage={formError} />}
      <Select 
        options={startLocationOptions}
        name="startLocationAddress"
        onChange={this.props.handleChange('startLocationAddress', this.handleStartLocationChange)}
        defaultValue={values.startLocationAddress}
        label='Select a Pick-up location'
        loading={startLocationOptionsLoading}
      />
      <Select
        options={numberOfCarsOptions}
        name="noOfCars"
        onChange={this.props.handleChange('noOfCars')}
        defaultValue={values.noOfCars}
        label='How many cars would you like to rent from the above location ?'
        loading={numberOfCarsOptionsLoading}
      />
      <Select 
        options={endLocationOptions}
        name="endLocationAddress"
        onChange={this.props.handleChange('endLocationAddress', this.handleEndLocationChange)}
        defaultValue={values.endLocationAddress}
        label='Where would you like to drop off the cars ?'
        loading={dropOffLocationOptionsLoading}
      />
      <ButtonWrapper>
        <Button onClick={this.back}>Back</Button>
        <Button onClick={this.saveAndContinue}>Save And Continue </Button>
      </ButtonWrapper>
    </Form>
    )
  }
}

export default LocationPreferences;