import React, { Component } from 'react';
import { Form } from 'semantic-ui-react';
import styled from 'styled-components';
import Field from '../molecules/Field';
import Button from '../../components/atoms/Button';
import { required, isNotEmail } from '../../utils/validations';
import ErrorBlock from '../../components/molecules/ErrorBlock';

const ButtonWrapper = styled.div`
  margin-top: 50px;
`

class UserDetails extends Component{
  constructor(props){
    super(props);
    this.state = {
      formError: '',
    }
  }

  saveAndContinue = (e) => {
    e.preventDefault();
    const {
      name,
      email,
    } = this.props.values;
    if(required(name)) {
      this.setState({
        formError: 'Name is Required',
      });
    } else if (required(email) || isNotEmail(email)) {
      this.setState({
        formError: 'Please enter a valid email',
      });
    } else {
      this.props.nextStep();
    }
  }

  back  = (e) => {
    e.preventDefault();
    this.props.prevStep();
  }

  render(){
    const { values } = this.props;
    const { formError } = this.state;
    return(
    <Form color='blue' >
      <h1 className="ui centered">Let us know how to contact you</h1>
      {formError && <ErrorBlock errorMessage={formError} />}
      <Field 
        type="text" 
        name="name"
        onChange={this.props.handleChange('name')}
        defaultValue={values.name}
      />
      <Field 
        type="text" 
        name="email"
        onChange={this.props.handleChange('email')}
        defaultValue={values.email}
      />
      <Field 
        type="text" 
        name="phoneNum"
        onChange={this.props.handleChange('phoneNum')}
        defaultValue={values.phoneNum}
      />
      <ButtonWrapper>
        <Button onClick={this.back}>Back</Button>
        <Button onClick={this.saveAndContinue}>Save And Continue </Button>
      </ButtonWrapper>
    </Form>
    )
  }
}

export default UserDetails;