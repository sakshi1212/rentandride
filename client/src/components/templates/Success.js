import React, { Component } from 'react';
import { Icon } from 'semantic-ui-react';
import styled from 'styled-components';

const StyledWrapper = styled.div`
  text-align: center;
`;

class Success extends Component{
  render(){
    return(
      <StyledWrapper>
        <Icon name='car' size='huge' circular inverted />
        <h1 className="ui">Your Booking has been confirmed. Happily Rent And Ride !</h1>
      </StyledWrapper>
    )
  }
}

export default Success;