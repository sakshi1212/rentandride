import React, { Component } from 'react';
import { Form } from 'semantic-ui-react';
import styled from 'styled-components';
import DateTimePicker from '../molecules/DateTimePicker';
import Button from '../../components/atoms/Button';
import ErrorBlock from '../../components/molecules/ErrorBlock';
import { required } from '../../utils/validations';

const ButtonWrapper = styled.div`
  margin-top: 50px;
`

class TimePreferences extends Component{
  constructor(props){
    super(props);
    this.state = {
      formError: '',
    }
  }

  saveAndContinue = (e) => {
    e.preventDefault();
    const {
      startTime,
      endTime,
    } = this.props.values;
    if(required(startTime)) {
      this.setState({
        formError: 'Please enter a valid start time',
      });
    } else if (required(endTime)) {
      this.setState({
        formError: 'Please enter a valid end time',
      });
    } else {
      this.props.nextStep();
    }
  }

  render(){
    const { values } = this.props;
    const { formError } = this.state;
    return(
      <Form>
        <h2>Enter your preferences</h2>
        {formError && <ErrorBlock errorMessage={formError} />}
        <DateTimePicker
          name="startTime"
          type="dateTime"
          value={values.startTime}
          onChange={this.props.handleChange}
          label="What time would you like to rent a car ?"
        />
        <DateTimePicker
          name="endTime"
          type="dateTime"
          value={values.endTime}
          onChange={this.props.handleChange}
          label="What time would you like to return the car ?"
        />
        <ButtonWrapper>
          <Button
            onClick={this.saveAndContinue}
          >
            Save And Continue
          </Button>
        </ButtonWrapper>
      </Form>
    )
  }
}

export default TimePreferences;