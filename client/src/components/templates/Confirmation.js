import React, { Component, Fragment } from 'react';
import { List } from 'semantic-ui-react';
import axios from 'axios';
import styled from 'styled-components';
import Button from '../../components/atoms/Button';
import ListItem from '../../components/molecules/ListItem';
import ErrorBlock from '../../components/molecules/ErrorBlock';

const ButtonWrapper = styled.div`
  margin-top: 50px;
`

const StyledList = styled(List)`
  &&& {
    margin: 20px 0px 10px;
  }
`

class Confirmation extends Component{
  constructor(props){
    super(props);
    this.state = {
      formError: '',
    }
  }

  saveAndContinue = (e) => {
    e.preventDefault();
    this.props.nextStep();
  }

  back  = (e) => {
    e.preventDefault();
    this.props.prevStep();
  }

  submitBooking = () => {
    const {
      email,
      name,
      phoneNum,
      startTime,
      endTime,
      startLocation,
      startLocationAddress,
      endLocation,
      endLocationAddress,
      noOfCars,
    } = this.props.values;

    axios.post('/bookings', {
      email,
      name,
      phoneNum,
      startTime,
      endTime,
      startLocation,
      startLocationAddress,
      endLocation,
      endLocationAddress,
      noOfCars,
    })
    .then(({ data }) => {
      this.props.nextStep();
    })
    .catch((err) => {
      this.setState({
        formError: `Error: ${err}`,
      });
      console.log(err);
    });
  }

  render(){
    const {values: { name, email, phoneNum, startTime, endTime, startLocationAddress, noOfCars, endLocationAddress }} = this.props;
    const { formError } = this.state;
    return(
      <Fragment>
        <h1 className="ui centered">Confirm your Details</h1>
        {formError && <ErrorBlock errorMessage={formError} />}
        <p>Click Confirm if the following details have been correctly entered</p>
        <StyledList>
          <ListItem iconName="user" label="name" content={name} />
          <ListItem iconName="user" label="Phone Number" content={phoneNum} />
          <ListItem iconName="mail" label="email" content={email} />
          <ListItem iconName="time" label="start Time" content={startTime} />
          <ListItem iconName="time" label="end Time" content={endTime} />
          <ListItem iconName="globe" label="start location" content={startLocationAddress} />
          <ListItem iconName="globe" label="end location" content={endLocationAddress} />
          <ListItem iconName="car" label="number of cars" content={noOfCars} />
        </StyledList>
        <ButtonWrapper>
          <Button onClick={this.back}>Back</Button>
          <Button type="submit" onClick={this.submitBooking}>Confirm</Button>
        </ButtonWrapper>
      </Fragment>
    )
  }
}

export default Confirmation;