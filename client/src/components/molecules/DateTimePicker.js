import React, { Fragment } from 'react';
import moment from 'moment';
import startCase from 'lodash/startCase';
import {
	DateInput,
	TimeInput,
	DateTimeInput,
	DatesRangeInput
} from 'semantic-ui-calendar-react';

const renderInput = ({
  name,
	type,
  props,
  value,
  onChange,
	...rest
}) => {
  switch (type) {
    case 'date': {
      return (
				<DateInput
					name={name || "date"}
          placeholder={startCase(name) || "Date"}
          value={value}
					iconPosition="left"
					onChange={onChange}
				/>
      )
    }
    case 'time': {
      return (
				<TimeInput
					name={name || "time"}
					placeholder={startCase(name) || "Time"}
					value={value}
					iconPosition="left"
					onChange={onChange}
				/>
      )
		}
		case 'dateTime': {
      return (
				<DateTimeInput
					name={name || "dateTime"}
          placeholder={startCase(name) || "Date Time"}
          initialDate={moment()}
          value={value || ''}
          minDate={moment().format('DD-MM-YYYY kk:mm')}
					iconPosition="left"
          onChange={onChange}
          closeOnMouseLeave
				/>
      )
		}
		case 'dateRange': {
      return (
				<DatesRangeInput
					name={name || "datesRange"}
					placeholder={startCase(name) || "From - To"}
					value={value}
					iconPosition="left"
					onChange={onChange}
				/>
      )
    }
    default: {
      return <div></div>
    }
  }
}

const DateTimePicker = ({
	type,
  name,
  label,
  ...rest,
}) => {
  return (
    <Fragment>
      <label>{label || startCase(name)}</label>
			{renderInput({ type, name, ...rest })}			
    </Fragment>
  )
};

export default DateTimePicker;
