import React from 'react';
import styled from 'styled-components';
import startCase from 'lodash/startCase'

const FieldWrapper = styled.div`
  margin-bottom: 15px;
`

const Field = ({
  type,
  name,
  placeholder,
  ...rest,
}) => {
  return (
    <FieldWrapper>
      <label>{startCase(name)}</label>
      <input
        type={type}
        name={name}
        placeholder={placeholder || startCase(name)}
        {...rest}
      />
    </FieldWrapper>
  )
};

export default Field;