import React from 'react';
import styled from 'styled-components';

const ErrorWrapper = styled.div`
  background: #fdd;
  border: 1px solid #efadad;
  padding: 10px;
  margin-bottom: 20px;
  border-radius: 4px;
`

const ErrorBlock = ({
  errorMessage
}) => {
  return (
    <ErrorWrapper>
      {errorMessage}
    </ErrorWrapper>
  )
};

export default ErrorBlock;