import React from 'react';
import startCase from 'lodash/startCase';
import { List } from 'semantic-ui-react';

const ListItem = ({
  iconName,
  label,
  content,
  ...rest,
}) => {
  return (
    <List.Item>
      <List.Icon name={iconName} />
      <List.Content><b>{startCase(label)}</b>: {content}</List.Content>
    </List.Item>
  )
};

export default ListItem;