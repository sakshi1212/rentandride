import React from 'react';
import styled from 'styled-components';
import startCase from 'lodash/startCase';
import toString from 'lodash/toString';
import isObject from 'lodash/isObject';
import { Dropdown } from 'semantic-ui-react';

const SelectWrapper = styled.div`
  margin-bottom: 15px;
`

const Select = ({
  name,
  options,
  isFluid = true,
  loading = false,
  placeholder,
  label,
  ...rest,
}) => {
  const selectOptions = options.map(o => {
    if (isObject(o)) {
      return {
        key: o.value,
        text: o.text,
        value: o.value,
      }
    }
    return {
      key: toString(o),
      text: toString(o),
      value: toString(o),
    }
  })
  return (
    <SelectWrapper>
      <label>{label || startCase(name)}</label>
      <Dropdown
        placeholder={placeholder || startCase(name)}
        fluid={isFluid}
        selection
        clearable
        loading={loading}
        options={selectOptions}
        {...rest}
      />
    </SelectWrapper>
  )
};

export default Select;
