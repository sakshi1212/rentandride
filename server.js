const express = require("express");
const bodyParser = require("body-parser");

require('./config/global')();
const configApi = require('./api');
const configSequelize = require('./config/sequelize');
const errorService = require('./services/error');

const app = express();
const server = require('http').createServer(app);
app.use(bodyParser.json());

configSequelize();
configApi(app);
app.use(errorService.middleware);
if (process.env.NODE_ENV === 'production') {
	app.use(express.static('client/build'));
}
app.get('*', (request, response) => {
	response.sendFile(path.join(__dirname, 'client/build', 'index.html'));
});
server.listen(process.env.PORT || 8080, () => {
  console.log(`Server listening on port ${process.env.PORT || 8080}!`);
});

module.exports = app;
